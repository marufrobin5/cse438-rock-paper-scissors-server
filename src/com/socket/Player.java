package com.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Player {
    public static Socket socket = null;
    public static void main(String[] args) {
        try {
            socket = new Socket("localhost", 8000);
            System.out.println("Connected to server\n" + "Socket: " + socket.getInetAddress() + ":" + socket.getPort() + "\n");

        } catch (IOException e) {
            System.out.println("Connection to network can not be stabilised ");
        }
        BufferedReader in = null;
        PrintWriter out = null;
        Scanner console_input = new Scanner(System.in);
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(),true);
            System.out.println("Server: "+ in.readLine());
            System.out.println("Client: Your Name______ ");
            out.println(console_input.nextLine());

            while (true) {
                System.out.println("Client: ");
                out.println(console_input.nextLine());

                System.out.println("Server: ");
                System.out.println(in.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
