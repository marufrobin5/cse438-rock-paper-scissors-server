package com.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissorsServer {
    public static int randomnumber(int min, int max)
    {
        if (min >= max) {
        throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    private static ServerSocket server = null;
    private static Socket socket = null;
    private static final int port = 8000;
    public static void main (String[] args){
        BufferedReader in = null;
        PrintWriter out = null;
        Scanner console_input = new Scanner(System.in);


        //Starting the server
        try {
            System.out.println("Server is staring ........");
            server = new ServerSocket(port);
            System.out.println("Server has Started");
        } catch (IOException e) {
            System.out.println("Can not listen to Port"+port);
            System.exit(-1);
        }
        while (true){
            try {
                socket = server.accept();
                System.out.println("Client has been connected\n");

            } catch (IOException e) {
                System.out.println("communication Error with Client");
                System.exit(-1);
            }
            try {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                out = new PrintWriter(socket.getOutputStream(),true);
                out.println("Hi! This is RockPaperScissorsServer.What is your Name?");

                System.out.println("Client Name: "+in.readLine());


                while (socket.isConnected())
                {//server taking random number 
                    int server_number = randomnumber(1,3);
                    //Scissors(3) > Paper(2) && paper(2)> Rock(1)&& Rock(1) > Scissors(3)
                    //Scissors3 beats paper2
                    int player_number = Integer.parseInt(in.readLine());
                    if (server_number == 1 && player_number ==2){
                        int Rock = server_number;

                        if (player_number > Rock)//Paper2 beats Rock1 {

                            out.println("Player Wins.");
                            System.out.println("Player Wins.");
//                        break;

                        }
                    }
                    if (server_number == 2 && player_number == 3 ) {
                        int Paper  = server_number;
                        if (player_number > Paper) {
                            //Scissors3 beats pasper2
                            out.println("Player Wins.");
                            System.out.println("Player Wins.");
//                        break;

                        }
                    }

                if (server_number == 3 && player_number == 1 ) {
                    int scissors  = server_number;
                    if (player_number > scissors) {
                        //Scissors3 beats pasper2
                        out.println("Player Wins.");
                        System.out.println("Player Wins.");
//                        break;

                    }
                }else {
                        out.println("Server Wins.");
                        System.out.println("Server Wins.");

                    }

//                    System.out.println("Server: ");
//                    out.println(console_input.nextLine());
//                    System.out.println("Client: ");
//                    System.out.println(in.readLine());
                }
            } catch (IOException e) {
                System.out.println("client left");
                console_input.close();
            }
        }
    }

}
